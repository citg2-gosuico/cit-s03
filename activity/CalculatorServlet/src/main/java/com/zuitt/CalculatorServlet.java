package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println("Calculator Servlet has benn initialized. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
//		getting the number inputs
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int result = 0;
		String operation = req.getParameter("operation");
		
//		condition
		
		if(operation.equals("add")) {
			result = num1 + num2;
		} else if (operation.equals("subtract")) {
			result = num1 - num2;
		} else if (operation.equals("multiply")) {
			result = num1 * num2;
		} else if (operation.equals("divide")) {
			result = num1 / num2;
		} 
		

		PrintWriter out = res.getWriter();
		out.println("The two numbers you provided are: " + num1 +", " + num2);
		out.println("The operation that you wanted is: " + operation);
		out.println("The result is: " + result);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("To use app, input TWO numbers and an OPERATION.<br><br>");
		out.println("Hit the submit button after filling in the details.<br><br>");
		out.println("You will get the result shown in your browser!");
		
		
	}

	public void destroy() {
		System.out.println("**************************************");
		System.out.println("Calculator Servlet has been destroyed.");
		System.out.println("**************************************");
	}
	
}
